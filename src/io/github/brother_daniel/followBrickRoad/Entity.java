package io.github.brother_daniel.followBrickRoad;

import java.awt.Graphics2D;
import java.awt.Image;

public class Entity {

	private float x;
	private float y;

	private Map map;
	private Image image;
	private float ang;
	private float size = 0.3f;

	public Entity(Image image, Map map, float x, float y, float size) {
		this.image = image;
		this.map = map;
		this.x = x;
		this.y = y;
		this.size = size;
	}

	public void paint(Graphics2D g) {
		// work out the screen position of the entity based on the
		// x/y position and the size that tiles are being rendered at. So
		// if we'e'd render on screen
		// at 15,15.

		int xp = (int) (Map.TILE_SIZE * x);
		int yp = (int) (Map.TILE_SIZE * y);

		// rotate the sprite based on the current angle and then
		// draw it
		g.rotate(ang, xp, yp);
		g.drawImage(image, (int) (xp - 16), (int) (yp - 16), null);
		g.rotate(-ang, xp, yp);
	}

	/**
	 * Move this entity a given amount. This may or may not succeed depending on
	 * collisions
	 * 
	 * @param dx
	 *            The amount to move on the x axis
	 * @param dy
	 *            The amount to move on the y axis
	 * @return True if the move succeeded
	 */
	public boolean move(float dx, float dy) {
		// Calculate new x and new y
		float nx = dx + x;
		float ny = dy + y;

		if (validLocation(nx, ny)) {
			// if it doesn't then change our position to the new position
			x = nx;
			y = ny;

			// and calculate the angle we're facing based on our last move
			ang = (float) (Math.atan2(dy, dx) - (Math.PI / 2));
			return true;
		}
		return false;
	}

	private boolean validLocation(float nx, float ny) {
		if (map.checkBounds(nx - size, ny - size)) {
			return false;
		}
		if (map.checkBounds(nx + size, ny - size)) {
			return false;
		}
		if (map.checkBounds(nx - size, ny + size)) {
			return false;
		}
		if (map.checkBounds(nx + size, ny + size)) {
			return false;
		}
		return true;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

}
