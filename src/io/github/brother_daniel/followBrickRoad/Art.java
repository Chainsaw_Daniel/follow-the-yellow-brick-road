package io.github.brother_daniel.followBrickRoad;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Art {
	private static Image sheet = loadImage("tex/spritesheet.png");
	private static Image logoSheet = loadImage("gui/logosheet.png");
	public static Image sprite = loadFromSheet(sheet, 0, 0, 32, 32);
	public static Image enemy = loadFromSheet(sheet, 1, 0, 32, 32);
	public static Image grass = loadFromSheet(sheet, 0, 1, 32, 32);
	public static Image yellowBrick = loadFromSheet(sheet, 1, 1, 32, 32);
	public static Image	finishLine = loadFromSheet(sheet, 2, 1, 32, 32);
	public static Image gameoverLogo = loadFromSheet(logoSheet, 0, 0, 32, 50);
	public static Image finishLogo = loadFromSheet(logoSheet, 0, 1, 32, 50);
	
	public static Image loadImage(String filename) {
		try {
			URL url = Thread.currentThread().getContextClassLoader()
					.getResource(filename);
			if (url == null) {
				System.err.println("Unable to find texture: " + filename);
				System.exit(0);
			}
			Image image;

			image = ImageIO.read(url);
			return image;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// we have to return this if there is an error it will cause the program
		// to crash but thats all we can do
		return null;
	}

	public static Image loadFromSheet(Image sheet, int col, int row,
			int height, int width) {
		if (sheet == null) { // error checking
			return sheet;
		}
		Image extractimg = ((BufferedImage) sheet).getSubimage(col * width, row * height, width,
				height); // extract from sheet
		return extractimg; // return img
	}

}
