package io.github.brother_daniel.followBrickRoad;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Game extends Canvas implements KeyListener {
	private static final long serialVersionUID = 1L;

	/** The buffered strategy used for accelerated rendering */
	private BufferStrategy strategy;

	private boolean left = false;
	private boolean right = false;
	private boolean up = false;
	private boolean down = false;

	private Map map;
	private Entity player;

	public boolean running = true;
	private boolean gameover = false;

	private boolean resetStage = false;

	public Game() {
		JFrame frame = new JFrame("Follow the Yellow Brick Road");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(this);
		setBounds(0, 0, 500, 500);
		frame.setSize(500, 500);
		frame.setResizable(false);
		// center the frame AFTER setting the size
		frame.setLocationRelativeTo(null);

		// add a key listener
		frame.addKeyListener(this);
		addKeyListener(this);

		// show frame
		frame.setVisible(true);

		resetStage();

	}

	private Map mainMap(Map map) {
		int WIDTH = 16;
		int HEIGHT = 15;
		int g = 0; // grass
		int b = 1; // brick
		int f = 2; // finish
		int[][] data =
			{ // tile map goes here
				{ g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g },
				{ g, b, b, b, b, g, b, b, b, g, b, b, b, g, g, g },
				{ g, g, g, g, b, g, b, g, b, g, b, g, b, g, g, g },
				{ g, g, g, g, b, g, b, g, b, g, b, g, b, g, g, g },
				{ g, g, g, b, b, g, b, g, b, g, b, g, b, b, g, g },
				{ g, b, b, b, g, b, b, g, b, g, b, g, g, b, g, g },
				{ g, b, g, b, g, b, g, g, b, g, b, g, b, b, g, g },
				{ g, b, b, b, g, b, g, b, b, g, b, g, b, g, g, g },
				{ g, g, b, g, g, b, g, b, g, b, b, g, b, g, g, g },
				{ g, g, b, b, b, b, g, b, g, b, g, g, b, g, g, g },
				{ g, g, g, g, g, g, g, b, g, b, g, g, b, g, g, g },
				{ g, g, g, g, g, b, b, b, g, b, g, g, b, g, g, g },
				{ g, g, g, g, g, b, g, g, g, b, g, g, b, b, b, g },
				{ g, g, g, g, g, b, b, b, b, b, g, g, g, g, b, g },
				{ g, g, g, g, g, g, g, g, g, g, g, g, g, g, f, g } };
		int a = Map.BLOCKED;
		int c = Map.CLEAR;
		int e = Map.FINISH;
		int[][] boundsMap =
			{
				{ a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a },
				{ a, c, c, c, c, a, c, c, c, a, c, c, c, a, a, a },
				{ a, a, a, a, c, a, c, a, c, a, c, a, c, a, a, a },
				{ a, a, a, a, c, a, c, a, c, a, c, a, c, a, a, a },
				{ a, a, a, c, c, a, c, a, c, a, c, a, c, c, a, a },
				{ a, c, c, c, a, c, c, a, c, a, c, a, a, c, a, a },
				{ a, c, a, c, a, c, a, a, c, a, c, a, c, c, a, a },
				{ a, c, c, c, a, c, a, c, c, a, c, a, c, a, a, a },
				{ a, a, c, a, a, c, a, c, a, c, c, a, c, a, a, a },
				{ a, a, c, c, c, c, a, c, a, c, a, a, c, a, a, a },
				{ a, a, a, a, a, a, a, c, a, c, a, a, c, a, a, a },
				{ a, a, a, a, a, c, c, c, a, c, a, a, c, a, a, a },
				{ a, a, a, a, a, c, a, a, a, c, a, a, c, c, c, a },
				{ a, a, a, a, a, c, c, c, c, c, a, a, a, a, c, a },
				{ a, a, a, a, a, a, a, a, a, a, a, a, a, a, e, a } };
		Image[] images = new Image[3];
		images[0] = Art.grass;
		images[1] = Art.yellowBrick;
		images[2] = Art.finishLine;
		int TILE_SIZE = 32;
		map = new Map(data, WIDTH, HEIGHT, images, boundsMap, TILE_SIZE);
		return map;
	}

	private void gameLoop() {
		long last = System.nanoTime();
		boolean gameoverSoundPlayed = false;
		while (running) {
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();

			// clear screen
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			if (gameover) {
				// check if hurt sound played
				if (gameoverSoundPlayed == false) {
					// if hurt sound wasnt played play it
					Sound.hurt.play();
					// set hurt sound to already played
					gameoverSoundPlayed = true;
				}
				map.paint(g);
				player.paint(g);

				// NOTE: The gameover logo must be drew after the map and player
				// so it's displayed on top of it otherwise we wouldn't see it
				Image original = Art.gameoverLogo;
				int newWidth = original.getWidth(null) * 3;
				int newHeight = original.getHeight(null) * 3;
				g.drawImage(original, getWidth() / 2 - newWidth / 2,
						getHeight() / 2 - newHeight / 2, newWidth, newHeight,
						this);

				// flip the buffer so we can see the rendering
				g.dispose();
				strategy.show();

				// pause a bit so that we don't choke the system
				try {
					Thread.sleep(4);
				} catch (Exception e) {
				}
				;
			} else {
				// if not yet finished
				map.paint(g);
				player.paint(g);
				if (map.checkFinish(player.getX(), player.getY())) {
					Sound.finish.play();
					Image original = Art.finishLogo;
					int newWidth = original.getWidth(null) * 3;
					int newHeight = original.getHeight(null) * 3;
					g.drawImage(original, getWidth() / 2 - newWidth / 2,
							getHeight() / 2 - newHeight / 2, newWidth,
							newHeight, this);
					running = false;
				}

				// flip the buffer so we can see the rendering
				g.dispose();
				strategy.show();

				// pause a bit so that we don't choke the system
				try {
					Thread.sleep(4);
				} catch (Exception e) {
				}
				;

				// calculate how long its been since we last ran the
				// game logic
				long delta = (System.nanoTime() - last) / 1000000;
				last = System.nanoTime();

				for (int i = 0; i < delta / 5; i++) {
					logic(5, g);
				}
				// after we've run through the segments if there is anything
				// left over we update for that
				if ((delta % 5) != 0) {
					logic(delta % 5, g);
				}
			}

			if (resetStage) {
				resetStage = false;
				resetStage();
				running = false; // stop the game loop
			}

		}
	}

	public void logic(long delta, Graphics2D g) {
		// check the keyboard and record which way the player
		// is trying to move this loop
		float dx = 0;
		float dy = 0;
		if (left) {
			dx -= 1;
		}
		if (right) {
			dx += 1;
		}
		if (up) {
			dy -= 1;
		}
		if (down) {
			dy += 1;
		}

		// if the player needs to move attempt to move the entity
		// based on the keys multiplied by the amount of time thats
		// passed
		if ((dx != 0) || (dy != 0)) {
			// if the player did not go to a valid location
			if (!(player.move(dx * delta * 0.003f, dy * delta * 0.003f))) {
				gameover = true;
			}
		}
	}

	public void resetStage() {
		running = false; // set running to false to make sure the game loop
							// stops

		left = false;
		right = false;
		up = false;
		down = false;

		running = true;
		gameover = false;

		// create the strategy used for accelerated rendering. More details
		// in the space invaders 2D tutorial
		createBufferStrategy(2);
		strategy = getBufferStrategy();

		map = mainMap(map);
		player = new Entity(Art.sprite, map, 1.5f, 1.5f, .4f);

		// start gameloop
		gameLoop();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			left = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			right = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			up = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			down = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			left = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			right = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			up = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			down = false;
		}
		// put in key released so it doesn't get spammed.
		if (e.getKeyCode() == KeyEvent.VK_R) {
			resetStage = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public static void main(String[] args) {
		new Game();
	}

}
