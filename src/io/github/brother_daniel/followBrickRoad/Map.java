package io.github.brother_daniel.followBrickRoad;

import java.awt.Graphics2D;
import java.awt.Image;

public class Map {
	/** The rendered size of the tile (in pixels) */
	public static int TILE_SIZE = 20;

	private int[][] map;
	private int mapWidth;
	private int mapHeight;
	private Image[] images; // textures

	public static int FINISH = 2;
	public static int BLOCKED = 1;
	public static int CLEAR = 0;
	private int[][] boundsMap;

	public Map(int[][] map, int mapWidth, int mapHeight, Image[] images,
			int[][] boundsMap, int tileSize) {
		this.map = map;
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.images = images;
		this.boundsMap = boundsMap;
		TILE_SIZE = tileSize;
	}

	public void paint(Graphics2D g) {
		for (int x = 0; x < mapHeight; x++) {
			for (int y = 0; y < mapWidth; y++) {
				Image image = images[map[x][y]];
				g.drawImage(image, y * TILE_SIZE, x * TILE_SIZE, TILE_SIZE,
						TILE_SIZE, null); // flipped map
			}
		}
	}

	public boolean checkBounds(float x, float y) {
		return boundsMap[(int) y][(int) x] == BLOCKED; // flip bounds map
	}
	
	public boolean checkFinish(float x, float y) {
		return boundsMap[(int) y][(int) x] == FINISH;
	}

}
